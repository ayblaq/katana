import { cardModel } from "../src/cards/cards.model";

export async function up() {
  const data = [
    { cardId: 1, value: "ACE", suit: "DIAMONDS", code: "AD" },
    { cardId: 2, value: "2", suit: "DIAMONDS", code: "2D" },
    { cardId: 3, value: "3", suit: "DIAMONDS", code: "3D" },
    { cardId: 4, value: "4", suit: "DIAMONDS", code: "4D" },
    { cardId: 5, value: "5", suit: "DIAMONDS", code: "5D" },
    { cardId: 6, value: "6", suit: "DIAMONDS", code: "6D" },
    { cardId: 7, value: "7", suit: "DIAMONDS", code: "7D" },
    { cardId: 8, value: "8", suit: "DIAMONDS", code: "8D" },
    { cardId: 9, value: "9", suit: "DIAMONDS", code: "9D" },
    { cardId: 10, value: "10", suit: "DIAMONDS", code: "10D" },
    { cardId: 11, value: "JACK", suit: "DIAMONDS", code: "JD" },
    { cardId: 12, value: "QUEEN", suit: "DIAMONDS", code: "QD" },
    { cardId: 13, value: "KING", suit: "DIAMONDS", code: "KD" },
    { cardId: 14, value: "ACE", suit: "CLUBS", code: "AC" },
    { cardId: 15, value: "2", suit: "CLUBS", code: "2C" },
    { cardId: 16, value: "3", suit: "CLUBS", code: "3C" },
    { cardId: 17, value: "4", suit: "CLUBS", code: "4C" },
    { cardId: 18, value: "5", suit: "CLUBS", code: "5C" },
    { cardId: 19, value: "6", suit: "CLUBS", code: "6C" },
    { cardId: 20, value: "7", suit: "CLUBS", code: "7C" },
    { cardId: 21, value: "8", suit: "CLUBS", code: "8C" },
    { cardId: 22, value: "9", suit: "CLUBS", code: "9C" },
    { cardId: 23, value: "10", suit: "CLUBS", code: "10C" },
    { cardId: 24, value: "JACK", suit: "CLUBS", code: "JC" },
    { cardId: 25, value: "QUEEN", suit: "CLUBS", code: "QC" },
    { cardId: 26, value: "KING", suit: "CLUBS", code: "KC" },
    { cardId: 27, value: "ACE", suit: "HEARTS", code: "AH" },
    { cardId: 28, value: "2", suit: "HEARTS", code: "2H" },
    { cardId: 29, value: "3", suit: "HEARTS", code: "3H" },
    { cardId: 30, value: "4", suit: "HEARTS", code: "4H" },
    { cardId: 31, value: "5", suit: "HEARTS", code: "5H" },
    { cardId: 32, value: "6", suit: "HEARTS", code: "6H" },
    { cardId: 33, value: "7", suit: "HEARTS", code: "7H" },
    { cardId: 34, value: "8", suit: "HEARTS", code: "8H" },
    { cardId: 35, value: "9", suit: "HEARTS", code: "9H" },
    { cardId: 36, value: "10", suit: "HEARTS", code: "10H" },
    { cardId: 37, value: "JACK", suit: "HEARTS", code: "JH" },
    { cardId: 38, alue: "QUEEN", suit: "HEARTS", code: "QH" },
    { cardId: 39, value: "KING", suit: "HEARTS", code: "KH" },
    { cardId: 40, value: "ACE", suit: "SPADES", code: "AS" },
    { cardId: 41, value: "2", suit: "SPADES", code: "2S" },
    { cardId: 42, value: "3", suit: "SPADES", code: "3S" },
    { cardId: 43, value: "4", suit: "SPADES", code: "4S" },
    { cardId: 44, value: "5", suit: "SPADES", code: "5S" },
    { cardId: 45, value: "6", suit: "SPADES", code: "6S" },
    { cardId: 46, value: "7", suit: "SPADES", code: "7S" },
    { cardId: 47, value: "8", suit: "SPADES", code: "8S" },
    { cardId: 48, value: "9", suit: "SPADES", code: "9S" },
    { cardId: 49, value: "10", suit: "SPADES", code: "10S" },
    { cardId: 50, value: "JACK", suit: "SPADES", code: "JS" },
    { cardId: 51, value: "QUEEN", suit: "SPADES", code: "QS" },
    { cardId: 52, value: "KING", suit: "SPADES", code: "KS" }
  ];

  await cardModel.insertMany(data)
}

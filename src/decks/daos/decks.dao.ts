import { v4 as uuidv4 } from 'uuid';
import { PatchDeckDto } from "../dto/patch.deck.dto";
import { CreateDeckDto } from '../dto/create.deck.dto';
import { deckModel } from '../decks.model';
import cardsDao from '../../cards/daos/cards.dao';

class DecksDao {
    DeckModel = deckModel;

    constructor() {}

    async addDeck(deckFields: CreateDeckDto) {
        const deckId = uuidv4();
        const deck = new this.DeckModel({
            deckId: deckId,
            cards: cardsDao.getCardIds(deckFields.type, deckFields.shuffled),
            ...deckFields,
        });

        await deck.save();

        return { deckId: deckId, remaining: deck.cards.length, ...deckFields };
    }

    async getDeckById(deckId: string) {
        return this.DeckModel.findOne({ deckId: deckId }, { __v: false, _id: false })
    }

    async openDeckById(deckId: string) {
        return this.DeckModel.findOne({ deckId: deckId }, { __v: false, _id: false }).then(async (deck: any) => {
            if (deck) {
                const cards = await cardsDao.getCards(deck.cards)
                return { deckId: deck.deckId, shuffled: deck.shuffled, remaining: deck.cards.length, type: deck.type, cards: cards };
            }

            return deck
        })
    }

    async drawDeckById(deckId: string, count: number) {
        return this.DeckModel.findOne({ deckId: deckId }, { __v: false, _id: false }).then(async (deck: any) => {
            const selection = deck.cards.slice(0, count);
            const remainder = deck.cards.slice(count, deck.cards.length)
            
            const cards = await cardsDao.getCards(selection)

            await this.updateDeckById(deckId, { cards: remainder })

            return {deckId: deckId, remaining: remainder.length, cards: cards};
        })
    }

    async updateDeckById(
        deckId: string,
        deckFields: PatchDeckDto
    ) {
        const existingDeck = await this.DeckModel.findOneAndUpdate(
            { deckId: deckId },
            { $set: deckFields },
            { new: true }
        ).exec();

        return existingDeck;
    }
}

export default new DecksDao();
import express from 'express';
import { body, ValidationError, validationResult } from 'express-validator';

class DecksValidators {
    errorFormatter = ({ location, msg, param, value, nestedErrors }: ValidationError) => {
        return `${msg}`;
    };

    validator = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const errors = validationResult(req).formatWith(this.errorFormatter);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.mapped() });
        next();
    };

    deckInputValidationRules = () => {
        return [
            body('type').isIn(["FULL", "SHORT"]).withMessage('must be either FULL or HALF'),
            body('shuffled').isBoolean().withMessage('expecting a boolean'),
            this.validator,
        ]
    }

    deckUuidValitdationRules = () => {
        return [
            body('deckId').isUUID().withMessage("expecting UUID"),
            this.validator,
        ]
    }

    deckDrawValitdationRules = () => {
        return [
            body('deckId').isUUID().withMessage("value should be UUID"),
            body('count').isInt().withMessage("value should be an integer"),
            this.validator,
        ]
    }
}

export default new DecksValidators();
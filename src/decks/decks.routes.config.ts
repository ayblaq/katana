import { CommonRoutesConfig } from '../common/common.routes.config';
import express from 'express';
import decksMiddleware from './middleware/decks.middleware';
import decksController from './controllers/decks.controller';
import decksValidators from './validators/decks.validators';

export class DecksRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'CardsRoutes');
    }

    configureRoutes() {
        this.app.route(`/api/v1/decks`)
            .post(
                decksValidators.deckInputValidationRules(),
                decksController.createDeck
            );

        this.app
            .route(`/api/v1/decks/open`)
            .get(
                decksValidators.deckUuidValitdationRules(),
                decksMiddleware.validateDeckExists,
                decksController.getDeckById
            )

        this.app
            .route(`/api/v1/decks/draw`)
            .get(
                decksValidators.deckDrawValitdationRules(),
                decksMiddleware.validateDeckExistsAndLimit,
                decksController.drawDeckById
            )

        return this.app;
    }
}
import { deckType } from "../../common/interfaces/common.interface";

export interface CreateDeckDto {
    shuffled: boolean,
    type: deckType
}
import express from 'express';
import decksService from '../../common/services/decks.service';

class DecksMiddleware {
    async validateDeckExists(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {

        const deck = await decksService.getById(req.body.deckId);
        if (deck) {
            next();
        } else {
            res.status(404).send({
                error: `DeckId does not exist`,
            });
        }
    }

    async validateDeckExistsAndLimit(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const deck = await decksService.getById(req.body.deckId);
        if (deck) {
            if (deck.remaining < req.body.count) {
                res.status(404).send({
                    error: `Specified number exceeds remainder: ${deck.remaining}`,
                });
            }
            next();
        } else {
            res.status(404).send({
                error: `Deck ${req.body.deckId} does not exist`,
            });
        }
    }
}

export default new DecksMiddleware();
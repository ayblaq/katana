import mongooseService from '../common/db/mongoose.service';
import { deckType } from '../common/interfaces/common.interface';


const Schema = mongooseService.getMongoose().Schema;

const deckSchema = new Schema({
    deckId: { type: String, required: true },
    shuffled: { type: Boolean, required: true },
    cards: { type: Array, required: true },
    type: { type: String, enum: deckType, default: 'FULL' },
});

export const deckModel = mongooseService.getMongoose().model('Decks', deckSchema)


import express from 'express';
import decksService from '../../common/services/decks.service';


class DecksController {
    async getDeckById(req: express.Request, res: express.Response) {
        const deck = await decksService.getById(req.body.deckId);
        res.status(200).send(deck);
    }

    async openDeckById(req: express.Request, res: express.Response) {
        const deck = await decksService.getById(req.body.deckId);
        res.status(200).send(deck);
    }

    async drawDeckById(req: express.Request, res: express.Response) {
        const deck = await decksService.drawDeckCardsById(req.body.deckId, req.body.count);
        res.status(200).send(deck);
    }

    async createDeck(req: express.Request, res: express.Response) {
        const deck = await decksService.create(req.body);
        res.status(200).send(deck);
    }
}

export default new DecksController();
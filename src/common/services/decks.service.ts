import decksDao from '../../decks/daos/decks.dao';
import { CRUD } from '../interfaces/crud.interface';
import { CreateDeckDto } from '../../decks/dto/create.deck.dto';

class DecksService implements CRUD {
    async create(resource: CreateDeckDto) {
        return decksDao.addDeck(resource);
    }

    async getById(deckId: string) {
        return decksDao.openDeckById(deckId);
    }

    async drawDeckCardsById(deckId: string, count: number) {
        return decksDao.drawDeckById(deckId, count);
    }
}

export default new DecksService();
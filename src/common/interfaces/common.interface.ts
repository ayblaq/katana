export enum deckType {
    FULL = "FULL",
    SHORT = "SHORT"
}

export interface deckCard {
    cardId: string;
    value: string;
    suit: string;
    code: string;
}

export interface deckCards extends Array<deckCard> { }

export interface Deck {
    deckId?: string;
    type: string;
    shuffled: boolean;
    cards?: Array<number> | deckCards | ArrayConstructor;
    remaining?: number;
}
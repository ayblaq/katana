import mongooseService from "../common/db/mongoose.service";

const Schema = mongooseService.getMongoose().Schema;

const cardSchema = new Schema({
    cardId: { type: Number },
    code: { type: String, require: true },
    suit: { type: String, require: true },
    value: { type: String, require: true }
})

export const cardModel = mongooseService.getMongoose().model('Cards', cardSchema);


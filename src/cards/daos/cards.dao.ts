import { cardModel } from '../cards.model';

class CardsDao {
    CardModel = cardModel;

    constructor() {
    }

    async getCards(cards: Array<number>) {
        return this.CardModel.aggregate([
            { $match: { cardId: { $in: cards } } },
            {
                $project: {
                    value: 1,
                    suit: 1,
                    code: 1,
                    index: {
                        $indexOfArray: [[0, ...cards], '$cardId'],
                    },
                },
            },
            { $sort: { index: 1 } },
            { $unset: ["index", "_id"] }
        ])
    }

    getCardIds(type: String, shuffled: boolean) {
        let cards: Array<number> = Array.from({ length: 52 }, (_, i) => i + 1)
        const short = [2, 3, 4, 5, 6, 15, 16, 17, 18, 19, 28, 29, 30, 31, 32,
            41, 42, 43, 44, 45]

        if (type == "SHORT") cards = cards.filter(x => !short.includes(x));
        if (shuffled) cards = cards.sort(() => Math.random() - 0.5);

        return cards
    }
}

export default new CardsDao();
import * as dotenv from 'dotenv'

const env = process.env.NODE_ENV || 'development'
dotenv.config({ path: `envs/${env}.env` });

export = {
    NODE_ENV : env,
    HOST : process.env.HOST || 'localhost',
    PORT : process.env.PORT || 3000,
    DB_URL : `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    DB_NAME: process.env.DB_NAME
}
import config from './config';
import express from 'express';
import * as http from 'http';
import cors from 'cors';
import debug from 'debug';

import { CommonRoutesConfig } from './src/common/common.routes.config';
import { DecksRoutes } from './src/decks/decks.routes.config';

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = config.PORT;
const routes: Array<CommonRoutesConfig> = [];
const debugLog: debug.IDebugger = debug('app');

app.use(express.json());

app.use(cors());

routes.push(new DecksRoutes(app));

const runningMessage = `Server running at http://localhost:${port}`;
app.get('/api/v1', (req: express.Request, res: express.Response) => {
    res.status(200).send(runningMessage)
});

export default server.listen(port, () => {
    routes.forEach((route: CommonRoutesConfig) => {
        debugLog(`Routes configured for ${route.getInfo()}`);
    });
    console.log(runningMessage);
});

import app from '../../app';
import supertest from 'supertest';
import mongoose from 'mongoose';
import { expect } from 'chai';
import config from '../../config';
import { v4 as uuidv4 } from 'uuid';

const invalidDeckType = {
    type: "HALF",
    shuffled: false,
};

const validDeckType = {
    type: "SHORT",
    shuffled: false,
};

const invalidDeckId = {
    deckId: uuidv4()
};

describe('validate deck creation input', () => {
    let request: supertest.SuperAgentTest;
    before((done) => {
        mongoose.connect(config.DB_URL, () => {
            mongoose.connection.dropDatabase(() => {
                done()
            })
        })
        request = supertest.agent(app);
    });
    after((done) => {
        app.close(() => {
            mongoose.connection.close(done);
        });
    });

    it('should not allow deck creation', async () => {
        const res = await request.post('/api/v1/decks').send(invalidDeckType);
        expect(res.status).to.equal(400);
        expect(res.body.errors.type).to.equal("must be either FULL or HALF");
    });

    it('should return deckId not valid', async () => {
        const res = await request.get('/api/v1/decks/open').send(invalidDeckId);
        expect(res.status).to.equal(404);
    });

    it('should allow deck creation', async () => {
        const res = await request.post('/api/v1/decks').send(validDeckType);
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.deckId).to.be.a('string');
    });
});

## About

This project was developed using Node v15.14.0.

## Available Scripts

This project can be deployed using docker through the following commands. 

#### `docker build .`
#### `docker-compose up -d`

The environmental variables are currently in env folder and currently uses the development.env file. 

## Without Docker

To build this project without docker, the following needs to be done:

#### download/install node and mongodb
#### make changes to environmental variables in development.env file
#### `npm install` to install dependencies
#### `npm run migrate:up` to run the initial database migration. 
#### `npm run start` to start the application. 


## API EndPoints

The api endpoints are:
#### `http://localhost:3000/api/v1/decks` to create the deck of cards (POST)
#### `http://localhost:3000/api/v1/decks/open` to open the created deck (GET)
#### `http://localhost:3000/api/v1/decks/draw` to draw some cards off the created deck (GET)

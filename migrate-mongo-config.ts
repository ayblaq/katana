import config from "./config"

export default {
  mongodb: {
    url: config.DB_URL,
    databaseName: config.DB_NAME,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  },
  migrationsDir: "migrations",
  changelogCollectionName: "changelog"
}
